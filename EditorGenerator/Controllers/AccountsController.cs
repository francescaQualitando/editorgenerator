/*
 * Controller for DB table Accounts
 * Created by http://editor.datatables.net/generator
 */
using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Web.Http;
using DataTables;
using EditorGenerator.Models;

namespace EditorGenerator.Controllers
{
    public class AccountsController : ApiController
    {
        [Route("api/Accounts")]
        [HttpGet]
        [HttpPost]
        public IHttpActionResult Accounts([FromBody] FormDataCollection formData)
        {
            var response = new Editor(WebApiApplication.Db, "Accounts")
                .Model<AccountsModel>()
                .Field(new Field("Email").Validator( Validation.NotEmpty() ).Validator( Validation.Email() ))
                .Field(new Field("Password").Validator( Validation.NotEmpty() ).Validator( Validation.MinLen( 6 ) )
                )
                .Process(formData)
                .Data();

            return Json(response);
        }
    }
}