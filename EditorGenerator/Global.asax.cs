﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using DataTables;

namespace EditorGenerator
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static Database Db { get; set; }

        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            // Make the Database connection
            Db = new Database(
               Properties.Settings.Default.DbType,
               Properties.Settings.Default.DbConnection
            );
        }
    }
}
