/*
 * Model for DB table Accounts
 * Created by http://editor.datatables.net/generator
 */
using DataTables;

namespace EditorGenerator.Models
{
    public class AccountsModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
    }
}