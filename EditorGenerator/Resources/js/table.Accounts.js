
/*
 * Editor client script for DB table Accounts
 * Created by http://editor.datatables.net/generator
 */

(function($){

$(document).ready(function() {
	var editor = new $.fn.dataTable.Editor( {
		"ajax": "/api/Accounts",
		"table": "#Accounts",
		"fields": [
			{
				"label": "Name",
				"name": "Name"
			},
			{
				"label": "Email",
				"name": "Email"
			},
			{
				"label": "Password",
				"name": "Password"
			}
		]
	} );

	$('#Accounts').DataTable( {
		"dom": "Tfrtip",
		"ajax": "/api/Accounts",
		"columns": [
			{
				"data": "Name"
			},
			{
				"data": "Email"
			},
			{
				"data": "Password"
			}
		],
		"tableTools": {
			"sRowSelect": "os",
			"aButtons": [
				{ "sExtends": "editor_create", "editor": editor },
				{ "sExtends": "editor_edit",   "editor": editor },
				{ "sExtends": "editor_remove", "editor": editor }
			]
		}
	} );
} );

}(jQuery));

